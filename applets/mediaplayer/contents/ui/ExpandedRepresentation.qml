import QtQuick 2.4
import QtQuick.Layouts 1.1
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 3.0 as PlasmaComponents3
import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.kcoreaddons 1.0 as KCoreAddons
import org.kde.kirigami 2.4 as Kirigami
import QtGraphicalEffects 1.0

Item {
    id: expandedRoot

    Layout.maximumHeight: units.gridUnit*10

    readonly property int durationFormattingOptions: Media.songLength >= 60*60*1000*1000 ? 0 : KCoreAddons.FormatTypes.FoldHours
    property bool disablePositionUpdate: false

    Rectangle { // Fallback for no album art
        anchors.fill: parent
        anchors.margins: -units.smallSpacing*2
        color: "#232629"
        z: !!Media.currentTrack ? 0 : 999
    }
    PlasmaExtras.Heading {
        visible: !Media.currentTrack
        anchors.centerIn: parent
        text: i18n("No Media Playing")
        color: "white"
        z: !!Media.currentTrack ? 1 : 1000
    }
    Image { // Background image.
        id: backgroundImage

        source: Media.albumArt
        sourceSize.width: 512 /* Setting a source size means the item doesn't need
                                to recompute blur as the user resizes the plasmoid
                                Additionally, it puts a bit of a cap on how large the
                                buffer getting blurred can be, saving resources.
                                */
        
        anchors.fill: parent
        anchors.margins: -units.smallSpacing*2
        fillMode: Image.PreserveAspectCrop
        
        asynchronous: true
        visible: !!Media.currentTrack && status === Image.Ready

        layer.enabled: true
        layer.effect: HueSaturation {
            cached: true

            lightness: -0.5
            saturation: 0.9

            layer.enabled: true
            layer.effect: GaussianBlur {
                cached: true

                radius: 128
                deviation: 12
                samples: 129

                transparentBorder: false
            }
        }
    }

    Column {
        anchors.fill: parent
        spacing: 0

        RowLayout {
            width: parent.width

            Image { // Album Art
                id: albumArt

                Layout.maximumWidth: expandedRoot.width / 4
                Layout.maximumHeight: units.gridUnit*5

                Layout.alignment: Qt.AlignTop

                visible: !!Media.currentTrack

                asynchronous: true

                horizontalAlignment: Image.AlignRight
                verticalAlignment: Image.AlignVCenter
                fillMode: Image.PreserveAspectFit

                source: Media.albumArt
            }

            ColumnLayout {
                Layout.alignment: Qt.AlignTop
                Layout.maximumHeight: units.gridUnit*5
                spacing: 0

                PlasmaExtras.Heading { // Song Title
                    id: songTitle
                    level: 1
                    
                    color: albumArt.visible ? "white" : PlasmaCore.ColorScope.textColor

                    textFormat: Text.PlainText
                    fontSizeMode: Text.HorizontalFit
                    maximumLineCount: 1

                    text: Media.currentTrack || i18n("No media playing")

                    Layout.maximumWidth: Math.round(expandedRoot.width * (3/4))
                }
                PlasmaExtras.Heading { // Song Artist
                    id: songArtist
                    visible: Media.currentTrack && Media.currentArtist

                    level: 2

                    color: albumArt.visible ? "white" : PlasmaCore.ColorScope.textColor

                    textFormat: Text.PlainText
                    fontSizeMode: Text.HorizontalFit
                    maximumLineCount: 1

                    text: Media.currentArtist

                    Layout.maximumWidth: Math.round(expandedRoot.width * (3/4))
                }
                PlasmaExtras.Heading { // Song Album
                    color: albumArt.visible ? "white" : PlasmaCore.ColorScope.textColor
                    
                    level: 3
                    opacity: 0.6
                    
                    textFormat: Text.PlainText
                    fontSizeMode: Text.HorizontalFit
                    maximumLineCount: 1

                    text: Media.currentAlbum

                    visible: text.length !== 0

                    Layout.maximumWidth: Math.round(expandedRoot.width * (3/4))
                }
            }
        }
        RowLayout { // Seek Bar
            spacing: units.smallSpacing

            width: parent.width

            enabled: !Media.noPlayers && !!Media.currentTrack && Media.songLength > 0 ? true : false

            opacity: enabled ? 1 : 0
            Behavior on opacity {
                NumberAnimation { duration: units.longDuration }
            }

            Item {
                Layout.fillWidth: true
                Layout.preferredHeight: seekSlider.height
                Layout.alignment: Qt.AlignVCenter

                PlasmaComponents3.ProgressBar { // Time Remaining
                    value: seekSlider.value
                    from: seekSlider.from
                    to: seekSlider.to
                    visible: !Media.canSeek
                    width: parent.width
                    anchors.verticalCenter: parent.verticalCenter
                }
            }

            PlasmaComponents3.Slider { // Slider
                id: seekSlider
                Layout.fillWidth: true
                z: 999
                value: 0
                visible: Media.canSeek
                to: Media.songLength

                onMoved: {
                    if (!disablePositionUpdate) {
                        // delay setting the position to avoid race conditions
                        queuedPositionUpdate.restart()
                    }
                }

                Timer {
                    id: seekTimer
                    interval: 1000 / Media.playbackRate
                    repeat: true
                    running: Media.state === "playing" && plasmoid.expanded && interval > 0 && seekSlider.to >= 1000000
                    onTriggered: {
                        // some players don't continuously update the seek slider position via mpris
                        // add one second; value in microseconds
                        if (!seekSlider.pressed) {
                            disablePositionUpdate = true
                            if (seekSlider.value == seekSlider.to) {
                                Media.retrievePosition();
                            } else {
                                seekSlider.value += 1000000
                            }
                            disablePositionUpdate = false
                        }
                    }
                }

                Connections {
                    target: Media
                    onSongLengthChanged: {
                        disablePositionUpdate = true
                        seekSlider.value = 0
                        Media.retrievePosition()
                        disablePositionUpdate = false
                    }
                    onPositionChanged: {
                        if (!seekSlider.pressed) {
                            disablePositionUpdate = true
                            seekSlider.value = Media.position
                            disablePositionUpdate = false
                        }
                    }
                }
            }
        }
        RowLayout {
            width: parent.width
            spacing: units.largeSpacing
            Kirigami.Theme.textColor: "white"

            PlasmaComponents3.ToolButton { // Previous
                enabled: Media.canGoPrevious
                Layout.preferredHeight: units.iconSizes.large
                Layout.preferredWidth: units.iconSizes.large
                contentItem: Kirigami.Icon {
                    source: LayoutMirroring.enabled ? "media-skip-forward" : "media-skip-backward"
                }
                onClicked: {
                    seekSlider.value = 0
                    Media.action(Media.previous)
                }
            }
            PlasmaComponents3.ToolButton { // Pause/Play
                enabled: Media.state == "playing" ? Media.canPause : Media.canPlay
                onClicked: Media.togglePlaying()
                Layout.preferredHeight: units.iconSizes.large
                Layout.preferredWidth: units.iconSizes.large
                contentItem: Kirigami.Icon {
                    source: Media.state == "playing" ? "media-playback-pause" : "media-playback-start"
                }
            }
            PlasmaComponents3.ToolButton { // Next
                enabled: Media.canGoNext
                Layout.preferredHeight: units.iconSizes.large
                Layout.preferredWidth: units.iconSizes.large
                contentItem: Kirigami.Icon {
                    source: LayoutMirroring.enabled ? "media-skip-backward" : "media-skip-forward"
                }
                onClicked: {
                    seekSlider.value = 0
                    Media.action(Media.next)
                }
            }
            Item {
                Layout.fillWidth: true
            }
            PlasmaComponents3.Label {
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignRight

                text: i18nc("Song played and duration, e.g. 0:20/5:23", "%1/%2",
                            KCoreAddons.Format.formatDuration((seekSlider.value) / 1000, expandedRoot.durationFormattingOptions),
                            KCoreAddons.Format.formatDuration((seekSlider.to) / 1000, expandedRoot.durationFormattingOptions))
                opacity: 0.9
                font: theme.smallestFont
                color: "white"
                visible: !!Media.currentTrack
            }
        }
    }

    Timer {
        id: queuedPositionUpdate
        interval: 200
        onTriggered: {
            if (Media.position == seekSlider.value) return
            Media.setPosition(seekSlider.value)
        }
    }
}
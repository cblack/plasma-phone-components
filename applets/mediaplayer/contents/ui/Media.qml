pragma Singleton

import QtQuick 2.0
import org.kde.plasma.core 2.0 as PlasmaCore

Item {
    id: media

    property var sources: []

    property alias source: mpris2Source

    property var current: mpris2Source.currentData || {}
    readonly property var currentMetadata: mpris2Source.currentData ? mpris2Source.currentData.Metadata : {}

    readonly property bool noPlayers: mpris2Source.sources.length <= 1

    readonly property var albumArt: currentMetadata ? currentMetadata["mpris:artUrl"] || "" : ""

    readonly property string currentTrack: {
        if (!currentMetadata) return ""

        var xesamTitle = currentMetadata["xesam:title"]
        if (xesamTitle) return xesamTitle

        var xesamUrl = currentMetadata["xesam:url"] ? currentMetadata["xesam:url"].toString() : ""
        if (!xesamUrl) return ""

        var lastSlashPos = xesamUrl.lastIndexOf('/')
        if (lastSlashPos < 0) return ""

        var lastUrlPart = xesamUrl.substring(lastSlashPos + 1)
        return decodeURIComponent(lastUrlPart)
    }
    readonly property string currentArtist: {
        if (!currentMetadata) return ""

        var xesamArtist = currentMetadata["xesam:artist"]
        if (!xesamArtist) return ""

        if (typeof xesamArtist == "string") {
            return xesamArtist
        }
        return xesamArtist.join(", ")
    }
    readonly property string currentAlbum: {
        if (!currentMetadata) return ""

        var xesamAlbum = currentMetadata["xesam:album"]
        if (xesamAlbum) return xesamAlbum

        if (currentMetadata["xesam:title"] || currentArtist) return ""

        var xesamUrl = (currentMetadata["xesam:url"] || "").toString()
        if (xesamUrl.indexOf("file:///") !== 0) return ""

        var urlParts = xesamUrl.split("/")
        if (urlParts.length < 3) return ""

        var lastFolderPath = urlParts[urlParts.length - 2] // last would be filename
        if (lastFolderPath) return lastFolderPath

        return ""
    }

    readonly property real playbackRate: current.Rate || 1
    readonly property double songLength: currentMetadata ? currentMetadata["mpris:length"] || 0 : 0
    readonly property bool canSeek: current.CanSeek || false

    readonly property bool canControl: (!noPlayers && current.CanControl) || false
    readonly property bool canGoPrevious: (canControl && current.CanGoPrevious) || false
    readonly property bool canGoNext: (canControl && current.CanGoNext) || false
    readonly property bool canPlay: (canControl && current.CanPlay) || false
    readonly property bool canPause: (canControl && current.CanPause) || false

    property double position: current.Position || 0

    readonly property int play: 0
    readonly property int pause: 1
    readonly property int next: 2
    readonly property int previous: 3

    function silentNull(i) {
        if (i == null) {
            return {}
        }
        return i
    }

    PlasmaCore.DataSource {
        id: mpris2Source

        readonly property string multiplexSource: "@multiplex"
                 property string currentSource: multiplexSource

        readonly property var currentData: data[currentSource]

        engine: "mpris2"
        connectedSources: sources

        onSourceAdded: updateSources()
        onSourceRemoved: updateSources()
    }

    function retrievePosition() {
        var service = mpris2Source.serviceForSource(mpris2Source.currentSource);
        var operation = service.operationDescription("GetPosition");
        service.startOperationCall(operation);
    }

    function setPosition(position) {
        var service = mpris2Source.serviceForSource(mpris2Source.currentSource)
        var operation = service.operationDescription("SetPosition")
        operation.microseconds = position
        service.startOperationCall(operation)
    }

    function updateSources () {
        let model = []

        for (var source in mpris2Source.sources) {
            if (source === mpris2Source.multiplexSource) return

            model.push({
                'source': source,
                'name': silentNull(mpris2Source.data[source])["Identity"],
                'icon': silentNull(mpris2Source.data[source])["Desktop Icon Name"] || silentNull(mpris2Source.data[source])["Desktop Entry"] || source
            })
        }

        media.sources = model
    }

    function serviceOp(src, op) {
        var service = mpris2Source.serviceForSource(src)
        var operation = service.operationDescription(op)
        service.startOperationCall(operation)
    }

    function action(act) {
        switch(act) {
            case Media.play:
                serviceOp(mpris2Source.currentSource, "Play")
                break
            case Media.pause:
                serviceOp(mpris2Source.currentSource, "Pause")
                break
            case Media.previous:
                serviceOp(mpris2Source.currentSource, "Previous")
                break
            case Media.next:
                serviceOp(mpris2Source.currentSource, "Next")
                break;
        }
    }
    
    function togglePlaying() {
        if (Media.state === "playing" && Media.canPause) {
            Media.action(Media.play)
        } else if (Media.canPlay) {
            Media.action(Media.pause);
        }
    }

    states: [
        State {
            name: "playing"
            when: !media.noPlayer && silentNull(media.current).PlaybackStatus === "Playing"
        },
        State {
            name: "paused"
            when: !media.noPlayer && silentNull(media.current).PlaybackStatus === "Paused"
        }
    ]
}